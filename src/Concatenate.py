from Usage import usage


def cat(args):
    """concatenate files and print on the standard output"""

    if len(args) == 0:
        usage(tool="cat")
    else:
        completeText = ""
        for fileName in args:
            textFile = open(fileName)
            completeText += textFile.read()
            textFile.close()
        print(completeText, end="")


def tac(args):
    """concatenate and print files in reverse"""

    if len(args) == 0:
        usage(tool="tac")
    else:
        completeText = ""
        for fileName in args:
            textFile = open(fileName)
            for line in reversed(list(textFile)):
                completeText += line
            textFile.close()
        print(completeText, end="")
