from Usage import usage


def wc(files):
    """print newline, word, and byte counts for each file"""

    if len(files) == 0:
        usage(tool="wc")
    else:
        byteCount, wordCount, lineCount = 0, 0, 0

        for fileName in files:
            textFile = open(fileName)
            for line in textFile:
                wordCount += len(line.split())
                byteCount += len(line)
                lineCount += 1
            textFile.close()
            print(lineCount, wordCount, byteCount, fileName)
