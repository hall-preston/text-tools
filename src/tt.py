#!/usr/bin/env python

from Concatenate import cat, tac
from CutJoin import cut, join
from Grep import grep, startGrep, endGrep
from Partial import head, tail
from Sorting import sort, uniq
from WordCount import wc
from Usage import usage
import sys

tools = {"cat": cat,
         "tac": tac,
         "cut": cut,
         "join": join,
         "grep": grep,
         "startGrep": startGrep,
         "endGrep": endGrep,
         "head": head,
         "tail": tail,
         "sort": sort,
         "uniq": uniq,
         "wc": wc
         }

TOOL_INDEX = 1
TOOL_ARGUMENT_INDEX = 2
toolArguments = sys.argv[TOOL_ARGUMENT_INDEX:]

if len(sys.argv) == 1:
    usage()
    sys.exit(1)
elif sys.argv[TOOL_INDEX] in tools:
    tools[sys.argv[TOOL_INDEX]](toolArguments)
else:
    usage(error="Tool is unavailable.")
