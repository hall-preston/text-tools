from Usage import usage


def grep(args):
    """print lines of files matching a pattern"""
    if len(args) == 0:
        usage(tool="grep")
    else:
        keyword = args[0]
        args = args[1:]

        for fileName in args:
            textFile = open(fileName)
            for line in textFile:
                if keyword in line:
                    print(line, end="")
            textFile.close()


def startGrep(args):
    """print lines of files matching a pattern"""
    if len(args) == 0:
        usage(tool="grep")
    else:
        keyword = args[0]
        args = args[1:]

        for fileName in args:
            textFile = open(fileName)
            for line in textFile:
                if line.startswith(keyword):
                    print(line, end="")
            textFile.close()


def endGrep(args):
    """print lines of files matching a pattern"""
    if len(args) == 0:
        usage(tool="grep")
    else:
        keyword = args[0]
        args = args[1:]

        for fileName in args:
            textFile = open(fileName)
            for line in textFile:
                line = line.strip()
                if line.endswith(keyword):
                    print(line, end="")
            textFile.close()