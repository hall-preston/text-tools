from Usage import usage


def head(args):
    """output the first part of files"""
    if len(args) == 0:
        usage(tool="head")
    else:
        headSize, fileName = getAmountAndFile(args)

        textFile = open(fileName)
        for line in textFile:
            print(line, end="")
            headSize -= 1
            if headSize == 0:
                break
        textFile.close()


def tail(args):
    """output the last part of files"""
    if len(args) == 0:
        usage(tool="tail")
    else:
        tailSize, fileName = getAmountAndFile(args)

        textFile = open(fileName)
        backwardsFileTail = []
        for line in reversed(list(textFile)):
            backwardsFileTail.append(line)
            tailSize -= 1
            if tailSize == 0:
                break
        for line in reversed(backwardsFileTail):
            print(line, end="")
        textFile.close()


def getAmountAndFile(args):
    if args[0] == "-n":
        return int(args[1]), args[2]
    else:
        return 10, args[0]
