from Usage import usage


def sort(args):
    """sort lines of text files"""
    if len(args) == 0:
        usage(tool="sort")
    else:
        for fileName in args:
            textFile = open(fileName)
            sortedFileList = sorted(list(textFile))

            for line in sortedFileList:
                print(line, end="")

            textFile.close()


def uniq(args):
    """report or omit repeated lines"""
    if len(args) == 0:
        usage(tool="uniq")
    elif args[0] == "-c":
        printDuplicateCount(args[1])
    elif args[0] == "-D":
        printLines(args[1], printDuplicates=True)
    elif args[0] == "-u":
        printLines(args[1])
    else:
        printSingleLines(args[0])


# ---------------------- FUNCTIONS FOR uniq --------------------------

# for default config of uniq
def printSingleLines(fileName):
    textFile = open(fileName)
    previousLines = []
    for line in textFile:
        if line not in previousLines:
            print(line, end="")
        previousLines.append(line)
    textFile.close()


# for '-c' config of uniq
def printDuplicateCount(fileName):
    previousLines = []  # stores lines up to the current one in the loop
    singleLines = []  # stores the first occurrence of the current line
    allOccurrences = []  # stores the occurrences of each line
    lineCount = 0
    occurrences = 1  # initialize counter variable for occurrences

    textFile = open(fileName)
    for line in textFile:
        if lineCount != 0:
            if line not in previousLines:
                singleLines.append(line)
                allOccurrences.append(occurrences)
                occurrences = 1
            else:
                occurrences += 1
        else:
            singleLines.append(line)
        previousLines.append(line)
        lineCount += 1
    allOccurrences.append(occurrences)
    textFile.close()

    # print results
    for i in range(len(allOccurrences)):
        print(allOccurrences[i], singleLines[i], end="")


# print duplicate or unique lines, depending on the state of printDuplicates - for "-D" and "-u" configs of uniq
def printLines(fileName, printDuplicates=False):
    printedLines = []

    fileAsList = list(open(fileName))
    for i in range(len(fileAsList)):
        if printDuplicates:  # printDuplicates flag is set to True
            if hasDuplicates(fileAsList[i], i, fileAsList) and fileAsList[i] not in printedLines:
                print(fileAsList[i], end="")
                printedLines.append(fileAsList[i])
        else:                # printDuplicates flag is set to False
            if not hasDuplicates(fileAsList[i], i, fileAsList) and fileAsList[i] not in printedLines:
                print(fileAsList[i], end="")
                printedLines.append(fileAsList[i])


# checks if a given string (line) has a duplicate in a given list
def hasDuplicates(line, lineIndex, fileList):
    for i in range(len(fileList)):
        if line == fileList[i] and i != lineIndex:
            return True
    return False



