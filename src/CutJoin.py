from Usage import usage
from Concatenate import cat


def cut(args):
    """remove sections from each line of files"""
    if len(args) == 0:
        usage(tool="cut")
        return
    elif args[0] == "-f":
        if "," in args[1]:
            columnIndexes = args[1].split(",")
        else:
            columnIndexes = [args[1]]
            print(columnIndexes)
        fileName = args[2]
    else:
        columnIndexes = ["0"]
        fileName = args[0]

    for i in range(len(columnIndexes)):
        columnIndexes[i] = int(columnIndexes[i])
    columnIndexes = sorted(columnIndexes)

    textFile = open(fileName)
    for line in textFile:
        lineList = line.split(",")
        for i in columnIndexes:
            if i >= len(lineList):
                print(lineList[0])
            elif "\n" in lineList[i]:
                print(lineList[i], end="")
            else:
                print(lineList[i])
    textFile.close()


def join(args):
    """merge lines of files"""
    if len(args) == 0:
        usage(tool="paste")
    elif len(args) == 1:
        cat(args)
    else:
        fileList = [""] * getMaxFileLength(args)

        for i in range(len(args)):
            textFile = open(args[i])
            textList = list(textFile)
            for j in range(len(textList)):
                if i == 0:
                    fileList[j] += textList[j].split("\n")[0]
                else:
                    fileList[j] += "," + textList[j].split("\n")[0]

        for line in fileList:
            print(line)


def getMaxFileLength(files):
    maxLength = 0
    for file in files:
        textFile = open(file)
        fileAsList = list(textFile)
        if len(fileAsList) > maxLength:
            maxLength = len(fileAsList)

    return maxLength
